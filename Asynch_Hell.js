users = ['sara','ali', 'lim'];
courses = [['java', 'js', 'html'], ['react', 'node'], ['python','django']];
grades = [[78, 75, 88], [85, 87], [91,92]];
function getData(data, function_resolved, function_rejected, error){
    if(error === undefined){
        error = false;
    }
    setTimeout(()=>{
        if(!error){
            function_resolved(data);
        }else{
            function_rejected(new Error('Error happened, uh oh'));
        }
    }, 3000);
}

getData(courses[2],(resp)=>{console.log(resp)}, (error)=>{error});