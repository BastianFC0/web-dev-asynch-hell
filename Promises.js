let users = ['sara','ali','lim'];
let courses = [['java','js','html'],['react','node'],['python','django']];
let grades = [[76,75,88],[85,87],[91,92]];
let getData = function(data, error=false){
    return new Promise((resolve,reject)=>{
        setTimeout(()=>{
            if(!error){
                resolve(data);
            }
            else{
                reject(new Error('something wrong'));
            }
        },1000)
    });
}
getData(users).then((resp)=>{
        let listUsers = resp;
        let user = listUsers.find((elem)=>{
            return elem == 'ali'
        });
        console.log('promise1, User is: ', user);
        return getData(courses);
}).then((resp)=>{
    console.log('promise2 - courses ', resp[1]);
    return getData(grades);
}).then((resp)=>{
    let avg = (resp[1][0]+resp[1][1])/2;
    console.log('promise3 - grades ', avg);
}).catch((err)=>{
        console.log('error', err);
});