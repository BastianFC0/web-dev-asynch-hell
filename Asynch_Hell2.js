//Part 1
let url = 'https://jsonplaceholder.typicode.com/todos/';
function testgetDataXHR(url){
    const xhr = new XMLHttpRequest;
    xhr.open('GET', url);
    xhr.send();
    xhr.addEventListener('load', ()=>{
        if(xhr.status === 200){
            let data = JSON.parse(xhr.response);
            for(let i=0;i<data.length;i++){
            if (/expedita/.test(data[i].title)){
            console.log(data[i].title);
            }
        }
        }else{
            console.log("Bad thing happened");
        }
    });
    xhr.addEventListener('error', ()=>{
        console.log('Network Error');
    });
}
testgetDataXHR(url);

//Part 2